import P from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import createLogger from 'debug';
import Camera from '../components/Camera';
import Canvas from '../components/HiddenCanvas';
import Countdown from '../components/Countdown';
import { obtainedVideo, obtainedCanvas, showReview } from '../actions';
import settings from '../common/settings';

const log = createLogger('pugsqueezers:camera-controller');

const Modes = Object.assign({}, Camera.Modes, {
  COUNTDOWN: 'countdown',
});

const videoSettings = {
  video: settings.camera,
};

function createOnComplete(dispatch) {
  return () => dispatch(showReview());
}

function createObtainCanvas(dispatch) {
  return (canvasElement) => {
    if (!canvasElement) return;
    dispatch(obtainedCanvas(canvasElement));
  };
}

function createObtainVideo(dispatch) {
  return (videoElement) => {
    if (!videoElement) return;
    const element = videoElement;
    log('video element', element);
    log('Obtaining video ->', videoSettings);
    dispatch(obtainedVideo(navigator.mediaDevices.getUserMedia(videoSettings)
                             .then((videoStream) => {
                               const url = window.URL.createObjectURL(videoStream);
                               log('Obtained video from', url);
                               element.srcObject = videoStream;
                               return {
                                 url,
                                 stream: videoStream,
                                 element: videoElement,
                               };
                             })));
  };
}

function CameraContainer(props) {
  return (<div>
    {props.mode === Modes.COUNTDOWN && <Countdown {...props} />}
    <Camera {...props} />
    <Canvas {...props} width={settings.window.width} height={settings.window.height} />
  </div>);
}

CameraContainer.propTypes = {
  mode: P.string.isRequired,
};

const mapStateToProps = ({ camera }) => ({
  mode: camera.mode,
  imageUrl: camera.lastPhotoUrl,
  videoUrl: camera.videoUrl,
  err: camera.err,
});

const mapDispatchToProps = dispatch => ({
  onComplete: createOnComplete(dispatch),
  obtainCanvas: createObtainCanvas(dispatch),
  obtainVideo: createObtainVideo(dispatch),
});

const CameraController = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CameraContainer);

CameraController.Modes = Modes;

export default CameraController;
