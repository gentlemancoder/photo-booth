import { connect } from 'react-redux';
import { showWelcome } from '../actions';
import Thankyou from '../components/Thankyou';

const mapStateToProps = state => ({
  visible: state.thankyou.open,
  timeout: state.app.thankyouTimeout,
});

const mapDispatchToProps = dispatch => ({
  onTimeout: () => dispatch(showWelcome()),
});

const ThankyouContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Thankyou);

export default ThankyouContainer;
