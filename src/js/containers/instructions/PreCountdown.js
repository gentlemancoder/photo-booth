import { connect } from 'react-redux';

import { PreCountdown } from '../../components/instructions';
import * as actions from '../../actions';

const mapStateToProps = ({ app, camera, sidebar }) => ({
  timeout: (app.previewDuration * 1000),
  numShots: app.photosPerSession,
  curShot: camera.photosTaken + 1,
  msgNum: sidebar.msgNum,
});

const mapDispatchToProps = dispatch => ({
  onTimeout: () => dispatch(actions.startCountdown()),
});

const Container = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PreCountdown);

export default Container;
