import { connect } from 'react-redux';

import { Review } from '../../components/instructions';
import * as actions from '../../actions';

const mapStateToProps = ({ app, camera, sidebar }) => ({
  timeout: (app.reviewDuration * 1000),
  numShots: app.photosPerSession,
  curShot: camera.photosTaken,
  msgNum: sidebar.msgNum,
});

const mapDispatchToProps = dispatch => ({
  onTimeout: () => {
    dispatch(actions.hideSidebar());
    setTimeout(() => dispatch(actions.showPreCountdown()), 500);
  },
});

const ReviewContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Review);

export default ReviewContainer;
