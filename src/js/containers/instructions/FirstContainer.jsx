import { connect } from 'react-redux';
import { FirstInstructions } from '../../components/instructions';
import * as actions from '../../actions';

const mapStateToProps = ({ app }) => ({
  img: app.logo,
  hasClicker: app.clickerEnabled,
  height: app.height,
  numShots: app.photosPerSession,
});

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(actions.showPreCountdown());
  },
});

const FirstContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FirstInstructions);

export default FirstContainer;
