import { connect } from 'react-redux';
import Sidebar from 'react-sidebar';

const mapStateToProps = state => ({
  open: state.sidebar.open,
  width: state.sidebar.width || 0,
  sidebar: state.sidebar.content,
  styles: {
    sidebar: {
      overflowY: 'visible',
    },
    content: {
      overflowY: 'hidden',
    },
    overlay: {
      backgroundColor: 'rgba(0,0,0,0.0)',
    },
  },
});

const mapDispatchToProps = () => ({});

const SidebarContainer = connect(mapStateToProps, mapDispatchToProps)(Sidebar);

export default SidebarContainer;
