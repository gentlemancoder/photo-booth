import { createAction } from 'redux-actions';
import canvasToBuffer from 'electron-canvas-to-buffer';

import settings from './common/settings';
import PhotoManager from './common/PhotoManager';

const photoManager = new PhotoManager(settings.app.saveLocation, '.png');

const obtainedVideo = createAction('OBTAINED_VIDEO');
const obtainedCanvas = createAction('OBTAINED_CANVAS');
const showPreCountdown = createAction('SHOW_PRECOUNTDOWN');
const showWelcome = createAction('SHOW_WELCOME');
const startCountdown = createAction('START_COUNTDOWN');
const hideSidebar = createAction('HIDE_SIDEBAR');

// we use a thunk here so that we can access the state
const showReview = () => (dispatch, getState) => {
  const state = getState();
  const video = document.getElementById(state.camera.videoId);
  const canvas = document.getElementById(state.camera.canvasId);
  const context = canvas.getContext('2d');
  context.drawImage(video, 0, 0);

  const buf = canvasToBuffer(canvas, 'image/png');
  dispatch(createAction('SHOW_REVIEW')(photoManager.savePhoto(buf)));
};

showReview.toString = () => 'SHOW_REVIEW';

export {
  obtainedVideo,
  obtainedCanvas,
  startCountdown,
  showPreCountdown,
  showReview,
  showWelcome,
  hideSidebar,
};
