import fs from 'fs';
import path from 'path';
import promisify from 'es6-promisify';

const writeFile = promisify(fs.writeFile);

function normalizeExtension(ext) {
  const extInternal = ext || '';
  return extInternal.startsWith('.') ? extInternal : `.${extInternal}`;
}

function initSaveLocation(loc) {
  const locInternal = path.resolve(process.cwd(), loc || '.');

  try {
    fs.accessSync(locInternal);
  } catch (e) {
    fs.mkdirSync(locInternal);
  }

  const stats = fs.statSync(locInternal);
  if (!stats.isDirectory()) {
    throw new Error(`ENOTDIR: ${locInternal}`);
  }

  return locInternal;
}

function findLastIndex(loc, ext) {
  // find all files that match the pattern
  return fs.readdirSync(loc)
           .filter(file => file.endsWith(ext))
           .map(file => +path.basename(file, ext))
           .reduce((curMax, curVal) => Math.max(curMax, curVal), 0);
}

class PhotoManager {
  constructor(fileLocation, ext) {
    // Ensure that the file location is a directory and exists
    // if not create it
    this.saveLocation = initSaveLocation(fileLocation);

    this.ext = normalizeExtension(ext);

    // determine next file available file index.
    this.nextIndex = findLastIndex(this.saveLocation, this.ext) + 1;
  }

  savePhoto(buffer) {
    const self = this;
    // determine next filename
    const filename = path.join(this.saveLocation, `${this.nextIndex}${this.ext}`);

    // save photo, calling cb when finished
    return writeFile(filename, buffer).then(() => {
      self.nextIndex += 1;
      return filename;
    });
  }

}

export default PhotoManager;
