import _ from 'lodash';
import createLogger from 'debug';

import userSettings from './settings.json';

const logger = createLogger('pugsqueezers:settings');
logger(`settings.json -> ${JSON.stringify(userSettings, null, 2)}`);

// import userSettings from './settings.json';
// these settings are applied if nothing overrides them in
// the settings.json file.
const defaultSettings = {
  window: {
    width: 800,
    height: 480,
  },
  camera: {
    fps: {
      min: 15,
      ideal: 30,
      max: 60,
    },
    width: { ideal: 800, min: 800, max: 800 },
    height: { ideal: 480, min: 480, max: 600 },
  },
  app: {
    clickerEnabled: false,
    countdownDuration: 3,
    previewDuration: 3,
    reviewDuration: 5,
    photosPerSession: 3,
    saveLocation: './photos',
  },
};
logger(`defaults -> ${JSON.stringify(defaultSettings, null, 2)}`);

// these settings cannot be overridden.
const hardSettings = {
  window: {
    frame: false,
    resizeable: false,
  },
};
logger(`unchangeable -> ${JSON.stringify(hardSettings, null, 2)}`);

const settings = _.merge({}
                        , defaultSettings
                        , userSettings
                        , hardSettings);

logger(`actualSettings -> ${JSON.stringify(settings, null, 2)}`);

export default settings;
