import createDebug from 'debug';

const debug = createDebug('pugsqueezers:isDevMode');

let tmpIsDevMode = true;

if (process.env.NODE_ENV) {
  tmpIsDevMode = !(/^production$/i).test(process.env.NODE_ENV);
} else {
  tmpIsDevMode = (/[\\/]electron/).test(process.execPath);
}

const isDevMode = tmpIsDevMode;

debug(`isDevMode: ${isDevMode}`);

export default isDevMode;
