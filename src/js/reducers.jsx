import React from 'react';
import { handleActions } from 'redux-actions';
import _ from 'lodash';

import * as actions from './actions';
import initialState from './initialState';
import Camera from './containers/CameraContainer';
import { FirstInstructions, PreCountdown, Review } from './containers/instructions';

const reducers = {};

reducers[actions.obtainedVideo] = {
  next: (state, action) => {
    if (state.camera.videoUrl) {
      return state;
    }
    return state.setIn(['camera', 'videoUrl'], action.payload.url)
                .setIn(['camera', 'videoStream'], action.payload.stream)
                .setIn(['camera', 'videoId'], action.payload.element.id);
  },
  throw: (state, action) => state.setIn(['camera', 'err'], action.payload),
};

reducers[actions.obtainedCanvas] = {
  next: (state, action) =>
    state.setIn(['camera', 'canvasId'], action.payload.id),
};

reducers[actions.showWelcome] = {
  next: state => state.setIn(['thankyou', 'open'], false)
                      .setIn(['sidebar', 'open'], true),
};

reducers[actions.showPreCountdown] = {
  next: (state) => {
    let ret = null;
    if (state.camera.photosTaken === state.app.photosPerSession) {
      ret = state.setIn(['sidebar', 'content'], <FirstInstructions />)
                 .setIn(['sidebar', 'open'], false)
                 .setIn(['thankyou', 'open'], true)
                 .setIn(['camera', 'photosTaken'], 0);
    } else {
      ret = state.setIn(['sidebar', 'content'], <PreCountdown />)
                 .setIn(['sidebar', 'open'], true)
                 .setIn(['sidebar', 'msgNum'], _.random(PreCountdown.messageCount - 1));
    }

    return ret.setIn(['camera', 'mode'], Camera.Modes.PREVIEW);
  },
};

reducers[actions.startCountdown] = {
  next: state => state.setIn(['sidebar', 'open'], false)
                      .setIn(['camera', 'mode'], Camera.Modes.COUNTDOWN),
};

reducers[actions.showReview] = {
  next: (state, action) => state.setIn(['sidebar', 'content'], <Review />)
                      .setIn(['sidebar', 'msgNum'], _.random(Review.messageCount - 1))
                      .setIn(['sidebar', 'open'], true)
                      .setIn(['camera', 'lastPhotoUrl'], action.payload)
                      .setIn(['camera', 'photosTaken'], state.camera.photosTaken + 1)
                      .setIn(['camera', 'mode'], Camera.Modes.REVIEW),
};
reducers[actions.hideSidebar] = {
  next: state => state.setIn(['sidebar', 'open'], false),
};

const reducer = handleActions(reducers, initialState);

export default reducer;
