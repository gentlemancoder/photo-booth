import P from 'prop-types';
import React from 'react';
import { Alert } from 'react-bootstrap';
import _ from 'lodash';

const baseStyle = {
  display: 'block',
  overflow: 'hidden',
};

const defaultVideoStyle = {
  position: 'relative',
  left: '50%',
  display: 'block',
};


class Video extends React.Component {
  constructor(...parms) {
    super(...parms);

    const videoStyle = _.merge({}, baseStyle, defaultVideoStyle);


    this.state = {
      videoId: Math.floor((1 + Math.random()) * 0x10000000).toString(16),
      videoStyle,
    };
  }

  render() {
    const containerStyle = Object.assign({}, baseStyle, this.props.style, { display: (this.props.src === null ? 'none' : 'block') });
    const videoStyle = Object.assign({}, this.state.videoStyle);

    videoStyle.transform = videoStyle.transform || '';

    if (videoStyle.position === 'relative') {
      videoStyle.transform += ' translateX(-50%)';
    }

    if (this.props.mirror) {
      videoStyle.transform += ' scaleX(-1)';
    }

    if (this.props.flip) {
      videoStyle.transform += ' scaleY(-1)';
    }

    // we wrap in a div because otherwise <video> causes a scrollbar if it is the
    // same size as the viewport in some browsers
    let content = null;
    if (this.props.error) {
      content = (<Alert bsStyle="danger">
        <h4>Unable to open video</h4>
        <p>{JSON.stringify(this.props.error)}</p>
        <code>{this.props.error.stack}</code>
      </Alert>);
    } else if (this.props.src === null) {
      content = (<Alert bsStyle="info">Loading video stream...</Alert>);
    }

    return (
      <div style={containerStyle}>
        {content}
        <video
          id={this.state.videoId}
          style={videoStyle}
          src={this.props.src}
          autoPlay={this.props.autoPlay}
          ref={this.props.obtainVideo}
        >
          <track kind="captions" />
        </video>
      </div>
    );
  }
}

Video.propTypes = {
  src: P.string,
  mirror: P.bool,
  flip: P.bool,
  autoPlay: P.bool,
  error: P.instanceOf(Object),
  obtainVideo: P.func,
  style: P.instanceOf(Object),
};

Video.defaultProps = {
  src: null,
  mirror: true,
  flip: false,
  autoPlay: true,
  error: null,
  obtainVideo: null,
  style: {},
};

export default Video;
