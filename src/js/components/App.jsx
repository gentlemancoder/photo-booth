import P from 'prop-types';
import React from 'react';

import Sidebar from '../containers/SidebarContainer';
import Camera from '../containers/CameraContainer';
import Thankyou from '../containers/ThankyouContainer';

const App = props => (
  <div>
    <Sidebar
      width={props.width}
      touch={false}
      sidebarClassName="sidebar"
      overlayClassName="sidebar-overlay"
      contentClassName="sidebar-content"
    >
      <Camera />
      <Thankyou />
    </Sidebar>
  </div>
);
App.propTypes = {
  width: P.number,
};
App.defaultProps = {
  width: 100,
};

export default App;
