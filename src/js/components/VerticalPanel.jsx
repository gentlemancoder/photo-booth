import P from 'prop-types';
import React from 'react';

const innerStyle = {
  position: 'relative',
  top: '50%',
  transform: 'translateY(-50%)',
};

const VerticalPanel = props => (
  <div style={innerStyle} {...props}>{props.children}</div>
);

VerticalPanel.propTypes = {
  children: P.node,
};

VerticalPanel.defaultProps = {
  children: null,
};

export default VerticalPanel;
