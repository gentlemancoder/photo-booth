import P from 'prop-types';
import React from 'react';
import SizedPanel from './SizedPanel';

const styles = {
  panel: {
  },
};

const Thankyou = (props) => {
  if (props.visible && props.timeout >= 0 && props.onTimeout) {
    setTimeout(props.onTimeout, props.timeout * 1000);
  }
  const panelStyle = Object.assign({}, styles.panel, { opacity: props.visible ? 1 : 0 });
  return (
    <SizedPanel className="ty-bg" style={panelStyle}>
      <div className="ty-msg">
        <div className="center middle">
          <h1 className="text-center">Thank you for sharing this very special day!</h1>
          <h2 className="text-center">Grab a business card to access your photos later</h2>
        </div>
      </div>
      <div className="ty-social">
        <h1 className="text-center">Photos will be available at:</h1>
        <div>
          <img className="ty-social-img center" src="images/blog.svg" alt="Blog" />
          <h2 className="text-center">www.pugsqueezers.ca</h2>
        </div>
        <div>
          <img className="ty-social-img center" src="images/instagram.svg" alt="Instagram" />
          <h2 className="text-center">@pug.squeezers</h2>
        </div>
      </div>
      <img className="ty-img" src="images/pugs.png" alt="Thank you" />
    </SizedPanel>
  );
};

Thankyou.propTypes = {
  visible: P.bool,
  timeout: P.number,
  onTimeout: P.func,
};

Thankyou.defaultProps = {
  visible: true,
  msg: null,
  panelClassName: null,
  msgClassName: null,
  imgClassName: null,
  imgSrc: null,
  onTimeout: null,
  timeout: 10,
};

export default Thankyou;
