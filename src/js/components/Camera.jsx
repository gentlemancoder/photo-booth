import P from 'prop-types';
import React from 'react';
import Video from '../components/Video';

const Modes = {
  PREVIEW: 'live-preview',
  REVIEW: 'review',
};

class Camera extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {
      styles: {
        container: {
          position: 'relative',
        },
        video: {
          top: 0,
          left: 0,
          opacity: 1,
        },
        image: {
          position: 'absolute',
          top: 0,
          left: 0,
          opacity: 1,
          transform: '',
        },
        flash: {
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          opacity: 1,
          backgroundColor: 'white',
          display: 'block',
        },
        toLivePreview: {
          // This style is used for the canvas and the video to ensure that
          // there is a smooth transition back to video mode after the image that
          // was just captured is shown.
          transition: 'all 0.8s ease-out',
        },
        hidden: {
          opacity: 0,
        },
        hiddenFlash: {
          display: 'none',
        },
      },
    };

    if (this.props.mirror) {
      this.state.styles.image.transform += ' scaleX(-1)';
    }

    if (this.props.flip) {
      this.state.styles.image.transform += ' scaleY(-1)';
    }

    this.obtainFlash = this.obtainFlash.bind(this);
    this.obtainImage = this.obtainImage.bind(this);
    this.updateImage = this.updateImage.bind(this);
    this.hideFlash = this.hideFlash.bind(this);
  }

  componentDidMount() {
    this.updateImage();
  }

  componentDidUpdate() {
    this.updateImage();
  }

  obtainFlash(flash) {
    this.state.flash = flash;
  }

  obtainImage(img) {
    this.state.img = img;
  }
  obtainTest(img) {
    this.state.test = img;
  }

  hideFlash() {
    if (this.state.flash) {
      this.state.flash.style.display = 'none';
    }
  }

  updateImage() {
    if (!this.state.img) {
      return;
    }

    const ctx = this.state.img.getContext('2d');
    if (this.props.mode === Modes.REVIEW) {
      const img = new Image();
      img.src = this.props.imageUrl;
      img.onload = () => ctx.drawImage(img, 0, 0);
    } else {
      // this prevents a black flash when transitioning back to live-preview mode
      setTimeout(() => ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight), 1000);
    }
  }

  render() {
    const imageStyle = this.props.mode === Modes.REVIEW
      ? Object.assign({}, this.state.styles.image)
      : Object.assign({}, this.state.styles.image
                        , this.state.styles.hidden
                        , this.state.styles.toLivePreview);

    const videoStyle = this.props.mode === Modes.REVIEW
      ? Object.assign({}, this.state.styles.video
                        , this.state.styles.hidden)
      : Object.assign({}, this.state.styles.video
                        , this.state.styles.toLivePreview);

    const flashStyle = this.props.mode === Modes.REVIEW
      ? Object.assign({}, this.state.styles.flash)
      : Object.assign({}, this.state.styles.flash
                        , this.state.styles.hiddenFlash);

    setTimeout(this.hideFlash, 100);

    return (
      <div style={this.state.styles.container}>
        <canvas
          style={imageStyle}
          width={window.innerWidth}
          height={window.innerWidth}
          ref={this.obtainImage}
        />
        <Video
          src={this.props.videoUrl}
          mirror={this.props.mirror}
          flip={this.props.flip}
          autoPlay={this.props.autoPlay}
          error={this.props.err}
          obtainVideo={this.props.obtainVideo}
          style={videoStyle}
        />
        <div style={flashStyle} ref={this.obtainFlash} />
      </div>
    );
  }
}

Camera.propTypes = {
  mode: P.string,
  videoUrl: P.string,
  imageUrl: P.string,
  mirror: P.bool,
  flip: P.bool,
  autoPlay: P.bool,
  obtainVideo: P.func,
  err: P.instanceOf(Object),
};

Camera.defaultProps = {
  mode: Modes.PREVIEW,
  videoUrl: null,
  imageUrl: null,
  mirror: true,
  flip: false,
  autoPlay: true,
  obtainVideo: null,
  err: null,
};

Camera.Modes = Modes;

export default Camera;
