import P from 'prop-types';
import React from 'react';
import _ from 'lodash';

const baseStyle = {
  display: 'none',
  overflow: 'hidden',
  transform: '',
};


class HiddenCanvas extends React.Component {
  constructor(...parms) {
    super(...parms);

    const canvasStyle = _.merge({}, baseStyle);

    if (this.props.mirror) {
      canvasStyle.transform += ' scaleX(-1)';
    }

    if (this.props.flip) {
      canvasStyle.transform += ' scaleY(-1)';
    }

    this.state = {
      canvasId: Math.floor((1 + Math.random()) * 0x10000000).toString(16),
      canvasStyle,
    };
  }

  render() {
    return (
      <canvas
        id={this.state.canvasId}
        style={this.state.canvasStyle}
        ref={this.props.obtainCanvas}
        width={this.props.width}
        height={this.props.height}
      />
    );
  }
}

HiddenCanvas.propTypes = {
  mirror: P.bool,
  flip: P.bool,
  width: P.number,
  height: P.number,
  obtainCanvas: P.func,
};

HiddenCanvas.defaultProps = {
  mirror: true,
  flip: false,
  width: null,
  height: null,
  obtainCanvas: null,
};

export default HiddenCanvas;
