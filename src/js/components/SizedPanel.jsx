import P from 'prop-types';
import React from 'react';

const defaultStyle = {
  display: 'block',
  overflow: 'visible',
};

const SizedPanel = (props) => {
  const innerStyle = Object.assign({},
                                  defaultStyle,
                                  props.style,
                                  { width: props.width, height: props.height },
                                  );

  return <div style={innerStyle} className={props.className}>{props.children}</div>;
};

SizedPanel.propTypes = {
  children: P.node,
  height: P.string,
  width: P.string,
  className: P.string,
  style: P.instanceOf(Object),
};

SizedPanel.defaultProps = {
  children: null,
  height: '100%',
  width: '100%',
  className: null,
  style: {},
};

export default SizedPanel;
