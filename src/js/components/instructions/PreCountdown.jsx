import P from 'prop-types';
import React from 'react';

import SizedPanel from '../SizedPanel';

const messages = [
  (<div className="middle center">
    <p className="text-left">Grab your</p>
    <p className="text-center">props and</p>
    <p className="text-right">get ready...</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Don&apos;t sweat it!</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Don your choice threads...</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Get decked out...</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Your bit is next!</p>
  </div>),
  (<div className="middle center">
    <p className="text-left">Meanwhile,</p>
    <p className="text-center">back at</p>
    <p className="text-right">the ranch...</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Hey!!</p>
    <p className="text-center">No bogarting!</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Jazzed yet?!</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Time to make the scene!</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">Think fast!</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">What&apos;s buzzin&apos;, cuzzin&apos;?</p>
  </div>),
  (<div className="middle center">
    <p className="text-center">What&apos;s your tale, nightingale?</p>
  </div>),
];

export default class PreCountdown extends React.Component {
  componentDidMount() {
    if (this.props.onTimeout) {
      setTimeout(this.props.onTimeout, this.props.timeout);
    }
  }

  render() {
    return (
      <SizedPanel>
        <p className="text-right text-highlight sidebar-counter-text">
          Photo: {this.props.curShot} / {this.props.numShots}
        </p>
        <div className="align-left sidebar-message-text">
          {messages[this.props.msgNum]}
        </div>
        <div>
          <img className="center align-bottom" src="images/pinup2.png" alt="Costumed Princess" />
        </div>
      </SizedPanel>
    );
  }
}

PreCountdown.messageCount = messages.length;

PreCountdown.propTypes = {
  timeout: P.number,
  onTimeout: P.func,
  numShots: P.number,
  curShot: P.number,
  msgNum: (props, propName, componentName) => {
    if (+props[propName] < 0 || +props[propName] >= messages.length) {
      return new Error(`Invalid prop \`${propName}\` supplied to \`${componentName}\`. It must be >= 0 and < ${messages.length}. Validation failed.`);
    }

    return null;
  },
};

PreCountdown.defaultProps = {
  timeout: 1000,
  onTimeout: null,
  numShots: 0,
  curShot: 0,
  msgNum: 0,
};
