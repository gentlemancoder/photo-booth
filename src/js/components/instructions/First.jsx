import P from 'prop-types';
import React from 'react';
import Panel from '../VerticalPanel';

const FirstInstructions = props => (
  <Panel onClick={props.onClick}>
    {(props.img) &&
      <div>
        <img src={props.img} alt="logo" width="100%" className="logo" />
      </div>
    }
    <div>
      <p className="text-center">To start taking your<br />{props.numShots} photos:</p>
      <p className="text-center text-highlight">Tap the logo</p>
    </div>
  </Panel>
);

FirstInstructions.propTypes = {
  img: P.string.isRequired,
  onClick: P.func,
  numShots: P.number,
};

FirstInstructions.defaultProps = {
  onClick: null,
  numShots: 0,
};

export default FirstInstructions;
