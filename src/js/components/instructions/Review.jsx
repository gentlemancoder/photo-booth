import P from 'prop-types';
import React from 'react';

import SizedPanel from '../SizedPanel';

const messages = [
  (<div className="center middle">
    <p className="text-center">Hey Daddy-O!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Well, cast an</p>
    <p className="text-center">eyeball on you!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Copasetic!</p>
  </div>),
  (<div className="center middle">
    <p className="text-left">Now that&apos;s</p>
    <p className="text-center">cooking</p>
    <p className="text-right">with steam!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Wow!!</p>
    <p className="text-center">Fat city!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Nice Jelly Roll!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Keen!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">Made in the shade!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">That was totally</p>
    <p className="text-center">on the stick!</p>
  </div>),
  (<div className="center middle">
    <p className="text-center">What a panic</p>
    <p className="text-center">and a half!</p>
  </div>),
];

class Review extends React.Component {
  componentDidMount() {
    if (this.props.onTimeout) {
      setTimeout(this.props.onTimeout, this.props.timeout);
    }
  }

  render() {
    return (
      <SizedPanel>
        <p className="text-right text-highlight sidebar-counter-text">
          Photo: {this.props.curShot} / {this.props.numShots}
        </p>
        <div className="align-left sidebar-message-text">
          {messages[this.props.msgNum]}
        </div>
        <div>
          <img className="sassy-girl align-bottom" src="images/pinup1.png" alt="Sassy" />
        </div>
      </SizedPanel>
    );
  }
}

Review.propTypes = {
  timeout: P.number,
  onTimeout: P.func,
  numShots: P.number,
  curShot: P.number,
  msgNum: (props, propName, componentName) => {
    if (+props[propName] < 0 || +props[propName] >= messages.length) {
      return new Error(`Invalid prop \`${propName}\` supplied to \`${componentName}\`. It must be >= 0 and < ${messages.length}. Validation failed.`);
    }

    return null;
  },
};

Review.defaultProps = {
  timeout: 1000,
  onTimeout: null,
  numShots: 0,
  curShot: 0,
  msgNum: 0,
};

export default Review;
