import P from 'prop-types';
import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

function increment(oldVal, step = 1) {
  return oldVal + step;
}

function canContinueIncrement(val, endVal) {
  return val <= endVal;
}

function decrement(oldVal, step = 1) {
  return oldVal - step;
}

function canContinueDecrement(val, endVal) {
  return val >= endVal;
}

class Countdown extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentValue: props.startValue,
      endValue: props.endValue,
      nextValue: props.startValue > props.endValue ? decrement : increment,
      canContinue: props.startValue > props.endValue ? canContinueDecrement : canContinueIncrement,
      step: props.step,
      onComplete: props.onComplete,
      onTick: props.onTick,
      key: Math.floor((1 + Math.random()) * 0x10000000).toString(16),
    };

    this.countdown = this.countdown.bind(this);
  }

  componentDidMount() {
    this.countdown();
  }


  countdown() {
    if (this.state.onTick) {
      this.state.onTick(this.state.currentValue);
    }

    const newVal = this.state.nextValue(this.state.currentValue, this.state.step);

    setTimeout(() => {
      if (this.state.canContinue(newVal, this.state.endValue)) {
        this.setState({
          currentValue: newVal,
        });
        this.countdown();
      } else if (this.state.onComplete) {
        this.state.onComplete();
      }
    }, 1000);
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName={this.props.transitionName}
        transitionEnter={false}
        transitionLeaveTimeout={1500}
      >
        <div className={this.props.transitionName} key={`${this.state.key}~${this.state.currentValue}`}>{this.state.currentValue}</div>
      </CSSTransitionGroup>
    );
  }
}

Countdown.propTypes = {
  transitionName: P.string,
  onComplete: P.func,
  onTick: P.func,
  startValue: P.number,
  endValue: P.number,
  step: (props, propName, componentName) => {
    if (+props[propName] <= 0) {
      return new Error(`Invalid prop \`${propName}\` supplied to \`${componentName}\`. It must be > 0. Validation failed.`);
    }

    return null;
  },
};

Countdown.defaultProps = {
  transitionName: 'countdown',
  onComplete: null,
  onTick: null,
  startValue: 3,
  endValue: 0,
  step: 1,
};

export default Countdown;
