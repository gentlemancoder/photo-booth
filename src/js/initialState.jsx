import React from 'react';
import Immutable from 'seamless-immutable';
import { FirstInstructions } from './containers/instructions';
import Camera from './components/Camera';
import settings from './common/settings';

const canvasId = Math.floor((1 + Math.random()) * 0x10000000).toString(16);

export default Immutable.from({
  camera: {
    mode: Camera.Modes.PREVIEW,
    videoUrl: null,
    lastPhotoUrl: null,
    err: null,
    videoId: null,
    canvasId,
    photosTaken: 0,
  },
  sidebar: {
    open: true,
    width: settings.window.width * 0.3,
    content: <FirstInstructions />,
    msgNum: 0,
  },
  thankyou: {
    open: false,
  },
  countdown: 0,
  app: Object.assign({}, settings.app, {
    width: settings.window.width,
    height: settings.window.height,
  }),
});
