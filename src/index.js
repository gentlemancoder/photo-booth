import { app, BrowserWindow } from 'electron';  // eslint-disable-line import/no-extraneous-dependencies
import installExtension, { REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } from 'electron-devtools-installer';
import { enableLiveReload } from 'electron-compile';
import createDebug from 'debug';
import settings from './js/common/settings';
import isDevMode from './js/common/isDevMode';

const debug = createDebug('pugsqueezers:bootstrapper');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

debug(`settings: ${JSON.stringify(settings, null, 2)}`);

if (isDevMode) {
  debug('Enabling live reload...');
  enableLiveReload({ strategy: 'react-hmr' });
}
const createWindow = async () => {
  debug('Creating window...');
  // Create the browser window.
  const winSettings = settings.window;
  winSettings.show = false;
  winSettings.backgroundColor = '#000000';
  mainWindow = new BrowserWindow(winSettings);
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });

  // and load the index.html of the app.
  const mainHtml = `file://${__dirname}/index.html`;
  debug(`Loading ${mainHtml}...`);
  mainWindow.loadURL(mainHtml);

  // Open the DevTools.
  if (isDevMode) {
    debug('Installing extensions...');
    await installExtension(REACT_DEVELOPER_TOOLS)
            .then(() => debug('\tInstalled React extension...'))
            .catch(err => debug(`\tError installing React Extension! ${err}`));
    await installExtension(REDUX_DEVTOOLS)
            .then(() => debug('\tInstalled Redux extension...'))
            .catch(err => debug(`\tError installing Redux extension! ${err}`));
    debug('Extensions installed, opening devtools window...');

    mainWindow.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    debug('Window closed.');
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  debug('All windows closed.');
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  debug('Window activated.');
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
