#!/bin/sh
PATH=/usr/local/bin:/usr/bin:/bin

cd `dirname $0`

NODE_ENV=production DISPLAY=:0 yarn start
